<audio loop  autoplay id="myAudio">
  <source src="candra.mp3">
</audio>

<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Jaldi:700,400' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400i' rel='stylesheet'>
	<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Teko" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">	
	
  	
			<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
		<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
		
		<link rel="stylesheet" href="css/magnific-popup.css"/>
		<link rel="stylesheet" href="css/style_1.css"> <!-- Resource style -->
		
		<!--gallery-->
		<link rel="stylesheet" href="css/style_gallery.css">
		<link rel="stylesheet" href="css/can.css">
		
		<link rel="stylesheet" href="css/fonts/css/font-awesome.css">
		<link rel="stylesheet" href="css/fonts/css/font-awesome.min.css">
	
			
	<title>Putri & Angga Wedding</title>
	<style>
		.float
		{
			position: fixed;
			width: 50px;
			height: 50px;
			top: 45px;
			right: 15px;
			background-color: #85705f;
			opacity: 0.8;
			color: #FFF;
			border-radius: 50px;
			text-align: center;
			padding-top:12px;	
			z-index: 1;
		}
	</style>
	
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
</head>
<body>

<div class='background'>
<img src='img/sandi.jpg' width='100%' height='100%'>

 <div class='screen'>
	 
	<section class="cd-slider-wrapper">
		<div class='window'>
			<ul class="cd-slider">
				<li class="visible">
				
					<a onclick="playAudio()" type="button" id='un-mute1' class='float'><img src='img/music-mute.png' width='30px;'></a>
					<a onclick="pauseAudio()" type="button" id='mute1'  class='float'><img src='img/music.png' width='30px;'></a> 
					
					<div style='display:block; clear:both; margin-top:1%; margin:auto;' class='savethedate'>Save The Date</div>
					<div style='display:block; clear:both; padding-top:2%; margin:auto;' class='forthewedding'>For The Wedding Of</div>
					<div style='display:block; clear:both; padding-top:3%; margin:auto;' class='mempelai'>Putri<br><span>&</span><br>Angga</div>
					<div style='display:block; clear:both; padding-top:3%; margin:auto;' class='date'>
						<div style='float:left; width:30%; text-align:right; padding-top:20px;'>Jan</div>
						<div style='float:left; width:40%; font-size:60px; font-family: Dancing Script, cursive;'>05</div>
						<div style='float:left; width:30%; text-align:left; padding-top:20px;'>2019</div>
					</div>
					<div style='display:block; clear:both; padding-top:3%; margin:auto; clear:both;' class='alamat'>
					GEDUNG SITI BAROKAH CONVENTION HALL<br>
 																<span style='font-size:14px;'>Jl. Kampus Kel. Mogolaing, Kec. Kotamobagu Barat</span>

 					</div>
						
				</li>
				
				<li>
					
				 	<a onclick="playAudio()" type="button" id='un-mute2' class='float'><img src='img/music-mute.png' width='30px;'></a>
					<a onclick="pauseAudio()" type="button" id='mute2'  class='float'><img src='img/music.png' width='30px;'></a>
					  
					<div style='display:block;' class='head'>Bride <span>&</span> Groom</div>
					<div style='display:block;' class='cpw-cpp'>
						<div class='cpw'>
							<div style='display:block; margin-top:3%;'>
							<img src='img/puput.png' width='150px;' class='img_bride_groom'>
							</div>
							<div style='display:block; margin-top:1%;' class='bride-groom'>
								Meita Liana Putriyani Arbie,SS							<span style='display:block;'>Anak Pertama dari<br>
Bpk. Ir. Jhony Arbie & Ibu Aisyah Kadengkang(Almh) </span>
							</div>
						</div>
						
						<div class='cpp'>
							<div style='display:block; margin-top:3%;'>
								<img src='img/angga.png' width='150px;' class='img_bride_groom'>
							</div>
							<div style='display:block; margin-top:3%;' class='bride-groom'>
								Erlangga Saputra Rantung, S.Kom							<span style='display:block;'>Anak Ketiga dari <br>
Bpk. Masrul Rantung & Ibu Yatty Kolopita</span>
							</div>
						</div>
					</div>
				</li>

				<li>
					<a onclick="playAudio()" type="button" id='un-mute3' class='float'><img src='img/music-mute.png' width='30px;'></a>
					<a onclick="pauseAudio()" type="button" id='mute3'  class='float'><img src='img/music.png' width='30px;'></a> 
					
					<div style='display:block;' class='head'>When <span>&</span> Where</div>
					<div style='display:block;' class='clock'>
						<div id="hitungmundur1" class='gaya'></div>
						<div id="hitungmundur2" class='gaya'></div>
						<div id="hitungmundur3" class='gaya'></div>
						<div id="hitungmundur4" class='gaya'></div>
					</div>
					<div style='display:block;' class='cpw-cpp'>
	 
						<div class='cpp'>
							<div style='display:block; margin-top:7%;' class='wedding-ceremony'>Wedding Reception</div>
							<div style='display:block; margin-top:2%; font-size:17px; letter-spacing: 2px;'class='wedding-ceremony'>
								<table style='margin:auto; width:70%; color:white;'>
									<tr>
										<td><i class='fa fa-calendar' style=' margin-bottom:2%'></i></td>
										<td style='text-align:left; padding-left:10px;'>
											<span>05 Januari 2019</span>
										</td>
									</tr>
									<tr>
										<td><i class='fa fa-clock-o' style='display:block; margin-bottom:2%'></i></td>
										<td style='text-align:left; padding-left:10px;'>
											<span>
											19:00											</span>
										</td>
									</tr>
									<tr>
										<td><i class='fa fa-map-marker' style='display:block; margin-bottom:2%'></i></td>
										<td style='text-align:left; padding-left:10px;'>
											<span>GEDUNG SITI BAROKAH CONVENTION HALL<br>
											<span style='font-size:14px;'>Jl. Kampus Kel. Mogolaing, Kec. Kotamobagu Barat</span>
											</span>
										</td>
									</tr>
								</table>
								
							</div>
						</div>
						
						
					</div>
				</li>
				<li>
					<a onclick="playAudio()" type="button" id='un-mute4' class='float'><img src='img/music-mute.png' width='30px;'></a>
					<a onclick="pauseAudio()" type="button" id='mute4'  class='float'><img src='img/music.png' width='30px;'></a>
					
					<div style='display:block;' class='head'>Location</div>
					
					<br>
					<div style='display:block; margin-top:10%;color:white; margin:auto;' class='direction'>
						<a style='color:#FFF ' href='https://www.google.com/maps/place/Mogolaing,+Kotamobagu+Bar.,+Kota+Kotamobagu,+Sulawesi+Utara/@0.7323402,124.3004841,16z/data=!3m1!4b1!4m13!1m7!3m6!1s0x0:0x0!2zMMKwNDQnMDguNiJOIDEyNMKwMTgnMDcuOCJF!3b1!8m2!3d0.735712!4d124.30216!3m4!1s0x327e3d14cb23b839:0x2ff9554db1bba7fd!8m2!3d0.7343406!4d124.3060487' target='blank'><button class='button putih'>Set Direction Here</button></a>
					</div>
					<div style='display:block; margin:auto;' class='map'>
					<!DOCTYPE html>
<html style='height:100%'>
  <head>
   
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style>
      #map-canvas { height:420px;}
	</style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnoeeEmBRf90KfK7VVcQWwMUz43zGtE-Y&amp;sensor=false" type="text/javascript"></script>
  </head>
	<body>
		<div id="map-canvas"></div>
	</body>

<!-- Mirrored from weweyanggiwedding.grinvitation.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Dec 2018 03:44:31 GMT -->
</html>

<script type="text/javascript">
        function initialize() {
    var mapOptions = {
      zoom: 15,
      center: new google.maps.LatLng(0.735712, 124.302160)
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
    
    setMarkers(map, beaches);
  }

  var beaches = [
    ['GEDUNG SITI BAROKAH CONVENTION HALL', 0.735712, 124.302160],
  ];

  function setMarkers(map, locations) {
    var shape = {
      coords: [1, 1, 1, 20, 18, 20, 18 , 1],
      type: 'poly'
    };
    var infoWindow = new google.maps.InfoWindow;
    for (var i = 0; i < locations.length; i++) {
      var beach = locations[i];
      var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: beach[4],
        shape: shape,
        title: beach[0],
        zIndex: beach[3]
      });
      var html = 'Lokasi : '+beach[0]+'<br/>Latitude : '+beach[1]+'<br/>Longitude : '+beach[2]+'';
      bindInfoWindow(marker, map, infoWindow, html);
    }
  }
  
  function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

  google.maps.event.addDomListener(window, 'load', initialize);
</script>
					</div>
									</li>

				<li>
					<a onclick="playAudio()" type="button" id='un-mute5' class='float'><img src='img/music-mute.png' width='30px;'></a>
					<a onclick="pauseAudio()" type="button" id='mute5'  class='float'><img src='img/music.png' width='30px;'></a> 
					<div style='display:block;' class='head'>Gallery</div>
					
						<section class="portfolio-section">
						'<!--<div class="container">
							<ul class="portfolio-filter controls">
								<li class="control" data-filter="all">All</li>
								<li class="control" data-filter=".image">Image</li>
								<li class="control" data-filter=".video">Video</li>
							</ul>
						</div>-->                      
						<div class="container-fluid p-md-0" style='margin:auto;'>
							<div class="row portfolios-area">
								<ul id="photoslist" class="photo_gallery_12">
										    <li style='width: 100%;padding: 1px;max-height:600px !important;' class='mix col-lg-12 col-md-12 image'>
												<center>
												<a rel='gallery-3' href='img/slider/1.jpg' class='portfolio-item' data-setbg='img/slider/1.jpg'>
													<img src='img/slider/1.jpg'>
												</a>
												</center>
											</li>
											 <li style='width: 100%;padding: 1px;max-height:600px !important;' class='mix col-lg-12 col-md-12 image'>
												<center>
												<a rel='gallery-3' href='img/slider/2.jpg' class='portfolio-item' data-setbg='img/slider/2.jpg'>
													<img src='img/slider/2.jpg'>
												</a>
												</center>
											</li>
											
										    <li style='width: 100%;padding: 1px;max-height:600px !important;' class='mix col-lg-12 col-md-12 image'>
												<center>
												<a rel='gallery-3' href='img/slider/5.jpg' class='portfolio-item' data-setbg='img/slider/5.jpg'>
													<img src='img/slider/5.jpg'>
												</a>
												</center>
											</li>
										
										    <li style='width: 100%;padding: 1px;max-height:600px !important;' class='mix col-lg-12 col-md-12 image'>
												<center>
												<a rel='gallery-3' href='img/slider/3.jpg' class='portfolio-item' data-setbg='img/slider/3.jpg'>
													<img src='img/slider/3.jpg'>
												</a>
												</center>
											</li>	
											
											
										    <li style='width: 100%;padding: 1px;max-height:600px !important;' class='mix col-lg-12 col-md-12 image'>
												<center>
												<a rel='gallery-3' href='img/slider/4.jpg' class='portfolio-item' data-setbg='img/slider/4.jpg'>
													<img src='img/slider/4.jpg'>
												</a>
												</center>
											</li>
											
											 
											
							 
								 <li style='width: 100%;padding: 1px;' class='mix col-lg-12 col-md-12 video'>
										<center>
											<div class='videocontainer'>
												<iframe width='100%' height='300' src='https://www.youtube.com/embed/J39KPpOkqjI' frameborder='0'></iframe>
											</div>
										</center>
									</li> 
										
								</ul>
							</div>
						</div>
					
				</li>
				<li>
					<a onclick="playAudio()" type="button" id='un-mute6' class='float'><img src='img/music-mute.png' width='30px;'></a>
					<a onclick="pauseAudio()" type="button" id='mute6'  class='float'><img src='img/music.png' width='30px;'></a>
					
					<div style='display:block;' class='head'>Friend Wishes</div>
					<div style='display:block;' class='wishes'>
						<div class="container1" style='display:block;'>
						  <form action='#' id='tambah_komentar2' method='post' enctype='multipart/form-data'>
 							<div class="row">
							  
								<input type="text" id="fname" name="nama" placeholder="name" required>
							  
							</div>
							
							<div class="row">
							  
								<textarea id="subject" name="isi_ucapan" placeholder="Wishes" style="height:100px"  required></textarea>
							  
							</div>
							<div class="row">
							  <input type="submit" id='submit' value="SEND" style='width:100%'>
							</div>
						  </form>
						</div>
													<div id='komentar' class="container2" style='display:block;'>
								
								 
																</div>
												</div>
				</li>
			</ul> <!-- .cd-slider -->
		
		</div>
		<ol class="cd-slider-navigation">
			<li class="selected">
				<a href="#0">
				<div>
					<img src='img/home.png' width='60%' style='padding-top:10%'>
				</div>
				<em>Home</em></a>
			</li>
			<li>
				<a href="#0">
				<div>
					<img src='img/ring.png' width='60%' style='padding-top:10%'>
				</div>
				<em>Mempelai</em></a>
			</li>
			<li>
				<a href="#0">
				<div>
					<img src='img/calender.png' width='60%' style='padding-top:10%'>
				</div>
				<em>Acara</em></a>
			</i>
			<li>
				<a href="#0">
				<div>
					<img src='img/gps.png' width='60%' style='padding-top:10%'>
				</div>
				<em>Map</em></a>
			</i>
			<li>
				<a href="#0">
				<div>
					<img src='img/gallery.png' width='60%' style='padding-top:10%'>
				</div>
				<em>Gallery</em></a>
			</i>
			<li>
				<a href="#0">
				<div>
					<img src='img/doa.png' width='60%' style='padding-top:10%'>
				</div>
				<em>Ucapan dan Doa</em></a>
			</i>
		</ol> <!-- .cd-slider-navigation -->
		
		<div class="cd-svg-cover" data-step1="M1402,800h-2V0.6c0-0.3,0-0.3,0-0.6h2v294V800z" data-step2="M1400,800H383L770.7,0.6c0.2-0.3,0.5-0.6,0.9-0.6H1400v294V800z" data-step3="M1400,800H0V0.6C0,0.4,0,0.3,0,0h1400v294V800z" data-step4="M615,800H0V0.6C0,0.4,0,0.3,0,0h615L393,312L615,800z" data-step5="M0,800h-2V0.6C-2,0.4-2,0.3-2,0h2v312V800z" data-step6="M-2,800h2L0,0.6C0,0.3,0,0.3,0,0l-2,0v294V800z" data-step7="M0,800h1017L629.3,0.6c-0.2-0.3-0.5-0.6-0.9-0.6L0,0l0,294L0,800z" data-step8="M0,800h1400V0.6c0-0.2,0-0.3,0-0.6L0,0l0,294L0,800z" data-step9="M785,800h615V0.6c0-0.2,0-0.3,0-0.6L785,0l222,312L785,800z" data-step10="M1400,800h2V0.6c0-0.2,0-0.3,0-0.6l-2,0v312V800z">
			<svg height='100%' width="100%" preserveAspectRatio="none" viewBox="0 0 1400 800">
				<title>SVG cover layer</title>
				<desc>an animated layer to switch from one slide to the next one</desc>
				<path id="cd-changing-path" d="M1402,800h-2V0.6c0-0.3,0-0.3,0-0.6h2v294V800z"/>
			</svg>
		</div> <!-- .cd-svg-cover -->
	</section> <!-- .cd-slider-wrapper -->
	
</div>
</div>
	
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/snap.svg-min.js"></script>

	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mixitup.min.js"></script>
	<script src="js/magnific-popup.min.js"></script>
	<script src="js/main1.js"></script>


<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="js/simplyCountdown.js"></script>
	<script src="js/main.js"></script> <!-- Resource jQuery -->
	<script src="js/komentar.js"></script> <!-- Resource jQuery -->
	<script>
		var d = new Date(new Date().getTime() + 200 * 120 * 120 * 2000);

		// default example
		simplyCountdown('.simply-countdown-one', {
			year: d.getFullYear(),
			month: d.getMonth() + 1,
			day: d.getDate()
		});

		//jQuery example
		$('#simply-countdown-losange').simplyCountdown({
			year: d.getFullYear(),
			month: d.getMonth() + 1,
			day: d.getDate(),
			enableUtc: false
		});
	</script>

	<script>
	Hitung();
	function Hitung()
	{
	tahun = 2019;
	bulan = 1;
	hari = 05;
	jam = 19;
	menit = 00;
	detik = 00;

	setTimeout(function()
	{
	tglTarget = new Date(tahun, (bulan - 1), hari, jam, menit, detik, 00);
	tglSkrg = new Date();
	tglSkrg = new Date(tglSkrg.getFullYear(), tglSkrg.getMonth(), tglSkrg.getDate(), tglSkrg.getHours(), tglSkrg.getMinutes(), tglSkrg.getSeconds(), 00, 00);
	var sisaHari = parseInt((tglTarget-tglSkrg)/86400000);
	var sisaJam = parseInt((tglTarget-tglSkrg)/3600000);
	var sisaMenit = parseInt((tglTarget-tglSkrg)/60000);
	var sisaDetik = parseInt((tglTarget-tglSkrg)/1000);
	detik = sisaMenit*60;
	detik = sisaDetik-detik;
	menit = sisaJam*60;
	menit = sisaMenit-menit;
	jam = sisaHari*24;
	jam = (sisaJam-jam) < 0 ? 0 : sisaJam-jam;
	hari = sisaHari;
	mulaiHitung(hari,jam, menit,detik,tahun);
	}, 1000);
	}

	function mulaiHitung(hari, jam, menit, detik, tahun){
	document.getElementById("hitungmundur1").innerHTML=""+hari+"<span>DAYS</span>";
	document.getElementById("hitungmundur2").innerHTML=""+jam+"<span>HOURS</span>";
	document.getElementById("hitungmundur3").innerHTML=""+menit+"<span>MINUTES</span>";
	document.getElementById("hitungmundur4").innerHTML=""+detik+"<span>SECONDS</span>";
	Hitung();
	}

	</script>
	
	<script>
	document.getElementById('mute1').style.display = 'none';
	document.getElementById('mute2').style.display = 'none';
	document.getElementById('mute3').style.display = 'none';
	document.getElementById('mute4').style.display = 'none';
	document.getElementById('mute5').style.display = 'none';
	document.getElementById('mute6').style.display = 'none';
	
	var x = document.getElementById("myAudio"); 
	function playAudio() { 
		x.play();
		document.getElementById('un-mute1').style.display = 'none';
		document.getElementById('un-mute2').style.display = 'none';
		document.getElementById('un-mute3').style.display = 'none';
		document.getElementById('un-mute4').style.display = 'none';
		document.getElementById('un-mute5').style.display = 'none';
		document.getElementById('un-mute6').style.display = 'none';
		document.getElementById('mute1').style.display = 'inline-block';
		document.getElementById('mute2').style.display = 'inline-block';
		document.getElementById('mute3').style.display = 'inline-block';	
		document.getElementById('mute4').style.display = 'inline-block';	
		document.getElementById('mute5').style.display = 'inline-block';	
		document.getElementById('mute6').style.display = 'inline-block';	
	} 
	function pauseAudio() { 
		x.pause();
		document.getElementById('mute1').style.display = 'none';
		document.getElementById('mute2').style.display = 'none';
		document.getElementById('mute3').style.display = 'none';
		document.getElementById('mute4').style.display = 'none';
		document.getElementById('mute5').style.display = 'none';
		document.getElementById('mute6').style.display = 'none';
		document.getElementById('un-mute1').style.display = 'inline-block';
		document.getElementById('un-mute2').style.display = 'inline-block';
		document.getElementById('un-mute3').style.display = 'inline-block';
		document.getElementById('un-mute4').style.display = 'inline-block';
		document.getElementById('un-mute5').style.display = 'inline-block';
		document.getElementById('un-mute6').style.display = 'inline-block';
	}
	 
	</script>
</body>
</html>